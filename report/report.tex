\documentclass[10pt,a4paper]{article}
\usepackage{amssymb,amsmath}
\usepackage{graphicx,subfigure}
\usepackage{times}

\setlength\parindent{0pt}

\title{
    UG3 Introduction to Vision and Robotics \\
    Robotics Assignment}
\author{Clemens Wolff \& Monica Paun}
\date{March 28\textsuperscript{th}, 2013}

\begin{document}
\maketitle

\section{Introduction} \label{s:intro}

One of the primary objectives of artificial intelligence is to create machines
capable of autonomous decisions such as robots being able to independently
perform some task in any given environment, without requiring any human
supervision. In order to achieve this, robots are equipped with sensors, which
enable them to develop awareness of their surroundings. \\

This report explores various methods to equip Khepera II robots with a limited
form of such independence. The methods are implemented in the C programming
language\footnote{The choice of C over MATLAB is justified by the fact that C
offers the most reliable interface to the robotics simulator Webots.} and
evaluated using a simulated version of the Khepera II within the robotics
simulator Webots.\\

Section \ref{s:methods} proposes an algorithm to equip the Khepera II with
awareness of its position within the world and a method to enable it to follow
obstacles of arbitrary shape. The methods are discussed and evaluated against
worlds containing obstacles of varying complexity (walls, tunnels, concave and
convex shapes, connected mazes, ...) in Section \ref{s:results}. Section
\ref{s:conclusion} concludes the report.\\


\section{Methods} \label{s:methods}

\subsection {The Khepera II}

\begin{figure}[ht]
    \centering
        \includegraphics[width=6cm]{img/khepera2.jpg}
    \caption{Schematic of the Khepera II robot}
    \label{fig:khepera2}
\end{figure}

Figure \ref{fig:khepera2} gives a schematic representation of the Khepera II
robot. The robot is very simple and consists of two differential wheels (brown)
- the robot's actuators - and eight sensors\footnote{In the following, the
$i^{th}$ sensor shall be denoted $sensor^i$ and its value sampled at time $t$
shall be $sensor_t^i$.} (blue, labelled 0-7).\\

Two modifications are made to the stock model: the robot is fitted with a pen to
allow for evaluation of its trajectory, and if the robot hits an object, it
activates a light on the side of the collision to facilitate detection of such
events.\\


\subsection {Internal modelling of location}

In order to increase the robot's awareness of its surroundings, the robot
should, at any point in time $t$, keep an estimate of its current position
$(x_t, y_t, \theta_t)$ where $x$ and $y$ are the robot's coordinates in
two-dimensional space, and $\theta$ is the robot's bearing\footnote{The point of
origin of this coordinate system, naturally, is the robot's initial position
and bearing.}.\\
Equation \ref{eq:odometry} gives a formula for updating the robot's internal
world state based on data from within the robot (this is called odometry).

\begin{equation} \label{eq:odometry}
    \begin{array}{rcl}
        \varpi_l &=& \frac{\omega}{\eta}\kappa_l \\
        \varpi_r &=& \frac{\omega}{\eta}\kappa_r \\
        \theta_t &=& \theta_{t-1} + \frac{\varpi_l - \varpi_r}{\Omega}\\
        x_t &=& x_{t-1} + \frac{\varpi_l + \varpi_r}{2} cos(\theta_t)\\
        y_t &=& y_{t-1} + \frac{\varpi_l + \varpi_r}{2} sin(\theta_t)\\
    \end{array}
\end{equation}

$\kappa_l$ and $\kappa_r$ are the number of ticks of the left and right
shaft encoders since the last time-step, $\eta$ is the encoder's resolution
(i.e. how many encoder ``ticks" correspond to one wheel revolution - here
$\eta = 100$), $\omega$ is the radius of the wheels, and $\Omega$ is the
distance between the wheels.\\

The use of shaft encoder values rather than absolute wheel speeds or angular
velocity increases the odometry's error-tolerance: the encoders only get
incremented if the the robot is physically moving. This is interesting since it
allows other parts of the robot's controller to rely more heavily on the results
computed by equation \ref{eq:odometry}.\\


\subsection {Finding the nearest obstacle}

In order to increase the robot's autonomy, we do not assume that the robot
starts close to an object it can follow. The robot will rather move straight
until it is reasonably sure that it has found an obstacle (say when $sensor_t^2
> \Delta \lor sensor_t^3 > \Delta$, where $\Delta$ is the distance the robot
should keep to the obstacle at any given time) and then proceeds to follow
that.\\


\subsection {Following an obstacle}

A basic algorithm to make the robot follow an obstacle to its right
\footnote{Making the robot follow an obstacle on its left instead of its right
is a trivial modification: simply swap the values of $v_t^r$ and $v_t^l$, and
swap any reference to a sensor with its twin by symmetry.}
is given by control theory's P-control shown in equation \ref{eq:p_control}.\\

\begin{equation} \label{eq:p_control}
    \begin{array}{rcl}
        v_t^l & = & K_p e_t + \Phi \\
        v_t^r & = & \Phi \\
        e_t  & = & \Delta - \delta_t
    \end{array}
\end{equation}

$v_t^l$ and $v_t^r$ are the speeds to which the left and right wheel
should be set at time $t$, $\Phi$ is the speed that the robot should try to
move at, and $\delta_t$ is the perceived distance between the obstacle and the
robot (say, given by $sensor_t^4$\footnote{$sensor^5$ and an average of
$sensor^4$ and $sensor^5$ where also considered, but $sensor^4$ gave the
best results. This is probably because $sensor^4$ is positioned at 45 degrees of
the robot's axis which means that, unlike for $sensor^5$, a decrease in
$sensor_t^4$ will always be caused by moving away from the obstacle and an
increase in $sensor_t^4$ will always be caused by moving towards the
obstacle.}). $e_t$ is thus the deviation from the desired distance to the
obstacle at time $t$ - a quantity this controller aims to minimise.\\

If the robot is certain that it has an obstacle in front of itself
(say, when $\frac{1}{2}(sensor_t^1 + sensor_t^2) > \Delta$), the adjustment in
formula \ref{eq:obstacle} will make the robot perform a right turn.\\

\begin{equation} \label{eq:obstacle}
    v_t^l = -\Psi
\end{equation}

$\Psi$ is some constant representing turn speed. \\

The beauty of this approach is that equations \ref{eq:p_control} and
\ref{eq:obstacle} are everything that is required in order for the robot to be
able to follow any sort of trajectory: obstacles in front of the robot will make
the robot turn until the algorithm in \ref{eq:p_control} is able to take back
over and pull the robot back onto a track following its initial path. \\

A second adjustment can be made in order for the obstacle-avoidance turns to be
less wide (and thus better adjusted to environments that can potentially
contain narrow corridors and obstacles on the left-hand side of the robot):\\

\begin{equation} \label{eq:obstacle_narrow}
    v_t^r = \Phi + K_t (\Phi - v_t^l)
\end{equation}

Note that this adjustment should only be done when the robot is currently
turning (say, when $\frac{1}{2}(sensor_t^4 + sensor_t^5) > \Delta$).\\
Equation \ref{eq:obstacle_narrow} adds an additional parameter, $K_t$, that has
to be tuned. However, the resulting motion is much more suitable for worlds
such as mazes or tunnels, greatly reducing the number of robot collisions, thus
justifying the increase in controller complexity.\\

In order for the robot to travel speedily, we also adopt equation
\ref{eq:min_speed}, assuring that the robot will never be slower than some
minimal speed $\phi$. \\

\begin{equation} \label{eq:min_speed}
    v_t^l = max(v_t^l, \phi)
\end{equation}

In theory, equation \ref{eq:p_control} with the adjustments in equations
\ref{eq:obstacle} and \ref{eq:obstacle_narrow} should suffice for flawless
robot navigation. Robotics, however, is more concerned with practical
considerations... and testing revealed that the robot would sometimes still get
stuck. In order to resolve such situations, an additional mode was added to the
controller: if the robot is sufficiently sure that it can't move, simply make
the robot spin around its axis as per equation \ref{eq:is_stuck_mode}. \\

\begin{equation} \label{eq:is_stuck_mode}
    \begin{array}{rcl}
        v_t^l & = & \xi \psi \\
        v_t^r & = & -\xi \psi \\
        \xi & = & \begin{cases}
            1  & sensor_t^4 + sensor_t^5 < sensor_t^0 + sensor_t^1 \\
            -1 & otherwise \end{cases}
    \end{array}
\end{equation}

One way to detect if the robot is stuck is to compare the past estimated
positions of the robot to its current location as per equation
\ref{eq:is_stuck}.\\

\begin{equation} \label{eq:is_stuck}
    stuck_t = \begin{cases}
        true & (sensor_t^0 + sensor_t^1 > 0 \lor
                sensor_t^4 + sensor_t^5 > 0) \land \\
             & \forall i \in [1\cdots\tau]:
                 x_t = x_{t-i} \land
                 y_t = y_{t-i} \land
                 \theta_t = \theta_{t-i} \\
        false & otherwise \end{cases}
\end{equation}

While other, more sophisticated, methods of robot control such as
D-control\footnote{$v_t^l \gets v_t^l - K_d \frac{d}{dt}e_t$} and
I-control\footnote{$v_t^l \gets v_t^l + K_i \int_0^t e_\varphi d\varphi$}
exist, they add more parameters to be determined experimentally and did not
significantly improve the robot's performance. Following Occam's Razor, we thus
only implement the controller described in \ref{eq:p_control} with the
refinements laid out in equations \ref{eq:obstacle} and
\ref{eq:obstacle_narrow}, minimising the points of failure of the robot. \\

This controller is powerful enough to follow any sort of closed shape (circles,
curves, tunnels, walls...); by extension, the controller is thus also able to
solve simply-connected mazes
\footnote{It is a well known fact that simply-connected mazes are topologically
equivalent to circles i.e. solving such a maze is simply following the
equivalent circle from start to end.}. Another possible approach to follow mazes
such as the ones considered in this report would be to move until the robot
detects an obstacle, turn ninety degrees away from the wall it is currently
following, move until it hits another wall, rotate ninety degrees undoing the
previous rotation, and repeating. This algorithm is simpler (thus less error
prone and easier to implement to good results) but not guaranteed to solve
arbitrary mazes and might be quite wasteful, making the robot travel a lot more
than it would normally have to if the maze is not narrow and the obstacles
small.\\

Table \ref{tbl:parameters} gives the (experimentally determined) values of the
various constants involved in the presented controller. \\

\begin{table} [t,h]
\caption{\label{tbl:parameters} Robot control constants}
\vspace{2mm}
\centerline{
\begin{tabular}{|c|c|}
\hline
    Constant &  Value \\
    \hline  \hline
    $\Delta$ &  250 \\
    $\Phi$ &  20 \\
    $\phi$ &  $\frac{\Phi}{5}$ \\
    $\Psi$   &  $\Phi - \phi$ \\
    $\psi$   &  $\frac{\Psi}{4}$ \\
    $K_p$    &  0.08 \\
    $K_t$    &  0.3 \\
    $\tau$   &  4\\
\hline
\end{tabular}}
\end{table}


\subsection{Homing}
By using the estimate location, the robot can rotate to face its starting point;
the trigonometric or anti-trigonometric sense is set so that all the rotations
of this kind are less than $180 ^{\circ}$. The homing algorithm can be described as follows:\\

\begin{itemize}
\item The robot will rotate to face home and then, based on its sensor reading, will go into wall following mode if there is an impending frontal or lateral collision.

\item The sense of movement while following the obstacle is set based on the rapport between the values of the left and right sensors; in order to successfully avoid an  obstacle, the sense of movement can only be changed after the robot will move for a while without following any obstacle, and thus we can infer it moved from an obstacle to another one. For the purpose of this algorithm we assume the obstacles have a convex polygon as section.

\item The robot should stay into the wall following mode until it moved for a distance greater than a set threshold. That way, the robot cannot get stuck by alternatively rotating itself to start wall following and to face home, without actually moving.

\item In the case, no impending collision is detected, the robot will move forward for a fixed number a steps. In case it gets stuck, it will immediately rotate and just after that move forward in order to free its frontal side.

\item After either the wall following step or the going forward step, the robot will face again its home and the whole process described here will happen again, until home is reached.

\item Home is considered reached when the robot is within a set distance threshold from the original position, as the odometry is not perfect and it cannot be expected to detect the exactly same spot. As odometry errors tend to add towards zero over time, even if the robot misses its home first time it moves near it, it is very probable it will successfully return to it somewhere in t he future.

\end{itemize}

\section{Results and discussion} \label{s:results}

\subsection{Subjective evaluation}

Figures \ref{fig:tunnel}, \ref{fig:circle}, \ref{fig:concave},
\ref{fig:straight}, and \ref{fig:maze} trace the robot's trajectory when
following objects of varying complexity. Subjectively speaking, the robot moves
quite smoothly and fairly regularly, except around sharp corners (as shown by
the larger than usual variation in trajectory in Figure \ref{fig:tunnel}).\\

It should be noted that the values of $\Delta$ and $K_p$ greatly influence the
robot's performance following different objects: narrow tunnels require
different parameters than sharp edges or concave/convex objects (the values in
Table \ref{tbl:parameters} representing the best possible compromise between the
different tasks). \\

\begin{figure}[ht]
    \centering
        \subfigure[After one lap] {
        \includegraphics[height=6cm]{img/trace/tunnel.png}}
        \subfigure[After multiple laps] {
        \includegraphics[height=6cm]{img/trace/tunnel_long.png}}
    \caption{Trajectory following a tunnel-like object}
    \label{fig:tunnel}
\end{figure}

\begin{figure}[ht]
    \centering
        \subfigure[After one lap] {
        \includegraphics[height=5cm]{img/trace/circle.png}}
        \subfigure[After multiple laps] {
        \includegraphics[height=5cm]{img/trace/circle_long.png}}
    \caption{Trajectory following a circular object}
    \label{fig:circle}
\end{figure}

\begin{figure}[ht]
    \centering
        \subfigure[After one lap] {
        \includegraphics[height=4cm]{img/trace/concave.png}}
        \subfigure[After multiple laps] {
        \includegraphics[height=4cm]{img/trace/concave_long.png}}
    \caption{Trajectory following a concave object}
    \label{fig:concave}
\end{figure}

\begin{figure}[ht]
    \centering
        \includegraphics[width=5cm]{img/trace/box.png}
    \caption{Trajectory following a straight object}
    \label{fig:straight}
\end{figure}

\begin{figure}[ht]
    \centering
        \subfigure[After one lap] {
        \includegraphics[height=9cm]{img/trace/maze.png}}
        \subfigure[After multiple laps] {
        \includegraphics[height=9cm]{img/trace/maze_long.png}}
    \caption{Trajectory following a maze}
    \label{fig:maze}
\end{figure}


\subsection{Odometry accuracy}

A classic way to evaluate a robot's odometry is to command the robot to perform
a certain motion and then to calculate the difference between that and the
executed motion. Figures \ref{fig:movement_accuracy} and
\ref{fig:rotation_accuracy} show the difference between commanded and observed
motion for varying rotations and straight moves.\\

\begin{figure}
    \noindent \centering
        \subfigure[Short Forward Motion] {
        \includegraphics[width=\linewidth]{img/error/movement_error.png}}
        \subfigure[Long Forward Motion] {
        \includegraphics[width=\linewidth]{img/error/movement_error_long.png}}
    \caption{\label{fig:movement_accuracy} Forward Motion Accuracy}
\end{figure}

\begin{figure}
    \noindent \centering
        \subfigure[Short Rotation] {
        \includegraphics[width=\linewidth]{img/error/rotation_error.png}}
        \subfigure[Long Rotation] {
        \includegraphics[width=\linewidth]{img/error/rotation_error_long.png}}
    \caption{\label{fig:rotation_accuracy} Rotation Accuracy}
\end{figure}

The robot rotates quite accurately, with errors being less than 1.5 percent
of the commanded motion for most rotational values. However, the error for small
turns of less than 75 degrees (i.e. the kind of turns the robot would usually
be commanded to execute), is significantly higher than this average. Luckily,
the error for 90 degree rotations (another frequent commanded turn) is very
low. Interestingly, the magnitude of the error between commanded and executed
rotation seems to be cyclic with a period of 60 degrees: for any $n \in
\mathbb{N}$, the errors in the interval
$[90 + 60n, 91 + 60n, \cdots, 90 + 60(n+1)[$ increase monotonically but the
errors at $90 + 60n$ and $90 + 60(n+1)$ are low and almost the same. \\

As expected, the absolute and relative errors in commanded forward motions
increases monotonically (but slowly) with the distance covered due to errors
accumulating and deviations in bearing leading to sideways drift. \\

A more task-specific evaluation of the robot's odometry is to make the robot
perform some complex task (e.g. move through the maze in Figure \ref{fig:maze})
and compute the distance between the actual and estimated position of the robot
at regular time intervals. Table \ref{tbl:odometry} reveals that the odometry
error increases rapidly over time. \\

This result is very much expected as the estimates of both $x_t$ and $y_t$ are
directly based on the estimated value of $\theta_t$. This means that any error
in $\theta$ will lead to errors in $x$ and $y$; situations causing the robot to
rotate frequently will thus negatively impact the robot's ability to locate
itself in the world.\\

However, this does not negatively impact the task-performance of the robot
since it is not heavily reliant on positional information. \\

\begin{table} [t,h]
\caption{\label{tbl:odometry} Accuracy of odometry over time over twenty 100 seconds simulations}
\vspace{2mm}
\centerline{
\begin{tabular}{|c|c|}
\hline
Seconds & Error (cm) \\
\hline  \hline
5 & 1.14 \\
10 & 2.24 \\
20 & 2.41 \\
30 & 3.08 \\
60 & 4.30 \\
100 & 4.15 \\
\hline
\end{tabular}}
\end{table}


\section{Conclusion} \label{s:conclusion}
This report presented a simple but effective controller guiding a Khepera II
robot along obstacles of varying complexity based on infrared sensor input and
odometry calculations.\\

Possible future improvements could be to adjust the controller parameters
dynamically (i.e. ``learning" them as the robot moves) which would remove the
need for choosing compromising values of $\Delta$ and $K_p$. Furthermore, it
would be interesting to adapt the motion algorithms to take advantage of the
apparently cyclic nature of the rotational error by preferring to turn in certain
fixed low-error angles rather than ad-hoc as the situation requires.\\

\end{document}
