// compilation constants
//#define __VERBOSE__
//#define __PEN__
#define __KEYBOARD__

// simulation constants
#define __WEBOTS__
//#define __UNSTABLE_CONTROLLER__
#define TIME_STEP 64
#define TIME_THRESHOLD (TIME_STEP * 50)
#define PREFER_RIGHT true

// includes
#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __WEBOTS__
    #include <webots/differential_wheels.h>
    #include <webots/distance_sensor.h>
    #include <webots/light_sensor.h>
    #include <webots/robot.h>
    #include <webots/led.h>
    #ifdef __PEN__
        #include <webots/pen.h>
    #endif
#else
    #include <unistd.h>
#endif

// exit codes
#define EX_SUCCESS 0
#define EX_INTERRUPT 1
#define EX_UNHANDLED_CASE 2

// robot constants - leds
#define NUM_LEDS 2
#define LED_OFF 0
#define LED_ON 1
#define LED_RIGHT 0
#define LED_LEFT 1
// robot constants - distance sensors
#define NUM_DISTANCE_SENSORS 8
#define NOT_STUCK 0
#define STUCK_RIGHT 1
#define STUCK_LEFT 2
#define STUCK_TIME 4
#define DIST_TO_WALL 250
#define VMAX 20
#define VMIN 4
#define VTURN 2
#define Kp 0.08
#define Kd 0.0
#define Kt 0.3
// robot constants - light sensors
#define NUM_LIGHT_SENSORS 8
// robot constants - odometry
#define AXLE_LENGTH 0.053
#define WHEEL_RADIUS 0.008
#define ENCODER_RESOLUTION 100.0
#define HOME_THRESH 0.1

// globals
#ifdef __WEBOTS__
    WbDeviceTag ds[NUM_DISTANCE_SENSORS];
    WbDeviceTag ls[NUM_LIGHT_SENSORS];
    WbDeviceTag led[NUM_LEDS];
    #ifdef __PEN__
        WbDeviceTag pen;
    #endif
#else
    #define ROBOT_FILE "/dev/ttyS0"
    FILE* ROBOT;
#endif
double dsensor[NUM_DISTANCE_SENSORS]; // distance sensor values
double prev_dsensor[NUM_DISTANCE_SENSORS];
double lsensor[NUM_LIGHT_SENSORS]; // light sensor values
double vr; // speed of left wheel
double vl; // speed of right wheel
double x_pos; // estimated x-position (meters)
double y_pos; // estimated y-position (meters)
double theta; // estimated bearing (radians)
double prev_x_pos[STUCK_TIME];
double prev_y_pos[STUCK_TIME];
double prev_theta[STUCK_TIME];
double prev_encoder_left;
double prev_encoder_right;
long simulation_time; // time since simulation start (ms)
bool follow_right; // true if robot should follow right wall
bool robot_running; // true if robot is in a control loop
int collisions_left;
int collisions_right;

// hack to recover any path
typedef struct node {
    double vl;
    double vr;
    struct node* next;
} Node;
Node* head;
Node* curr;

// helper functions
void follow_wall(void);
void follow_move_history(void);
void odometry(void);
int is_stuck(void);
void remember_move(void);
void update_sensors(void);
void turn(double degrees);
void forwards(double meters);
void faceHome(void);
double trig_dist(double start, double stop);

// functions to abstract differences between webots/real robot
void init(void);
void cleanup(void);
void simulation_step(void);
void update_distance_sensors(void);
void update_light_sensors(void);
void set_led(int led_num, int value);
void leds_off(void);
void get_encoders(double* left, double* right);
void set_encoders(double left, double right);
void set_speeds(double left, double right);

// mode switching
void check_follow_wall_end(void);
void check_going_home_end(void);

// debugging functions
void enable_pen(int color);
void disable_pen(void);
void process_user_input(void);

#define avg(A, B) ((A + B) / 2.0)
#define max(A, B) (A > B ? A : B)
#define rad2deg(A) (A * 180.0 / M_PI)
#define deg2rad(A) (A * M_PI / 180.0)
#define dbl_eq(A, B) (fabs(A - B) <= DBL_EPSILON)
#define mod(A, B) (B == 0 ? A : A - B * floor(A / B))
#define M_2PI (2.0 * M_PI)
#define trig_dist(A, B) (B < A ? 360 + B - A : B - A)

#ifdef __VERBOSE__
    #define vprintf(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define vprintf(fmt, ...)
#endif
