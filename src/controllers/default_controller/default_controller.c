#include "default_controller.h"

int main(int argc, char* argv[]) {
    printf("***** 3, 2, 1, let's jam ! *****\n");
    printf("***** Press 'X' to return home ! *****\n");
    init();
    simulation_step();
    enable_pen(0xff0000);

    // find a wall
    printf("***** Find an obstacle ! *****\n");
    robot_running = true;
    while(robot_running) {
        odometry();
        update_sensors();
        vr = VMAX;
        vl = VMAX;
        set_speeds(vl, vr);
        // make a step
        simulation_step();
        double front_sensors  = avg(dsensor[2], dsensor[3]);
        if (front_sensors > DIST_TO_WALL) {
            robot_running = false;
        }
        check_follow_wall_end();
    }

    // follow the wall
    printf("***** Entering wall following mode ! *****\n");
    robot_running = true;
    while (robot_running) {
        odometry();
        update_sensors();
        follow_wall();
        simulation_step();
        check_follow_wall_end();
    }

    // return home
    printf("***** Entering homing mode ! *****\n");
    vprintf("x_pos: %f, y_pos: %f \n", x_pos, y_pos);
    enable_pen(0x0000ff);
    bool robot_close_to_home = false;
    bool chose_dir = false;
    while(!robot_close_to_home) {
        faceHome();
        update_sensors();
        double front_sensors  = avg(dsensor[2], dsensor[3]);
        if (!chose_dir) {
            if (dsensor[1] > dsensor[4]) {
                follow_right = false;
            } else {
                follow_right = true;
            }
            chose_dir = true;
            vprintf("Chose direction for an obstacle! \n");
        }
        // move forward a little
        if (front_sensors > DIST_TO_WALL || dsensor[1] > 200 ||
            dsensor[4] > 200) {
            // when blocked, move around the obstacle
            if (follow_right) {
                vprintf("Found obstacle and will follow with the right! \n");
            } else {
                vprintf("Found obstacle and will follow with the left! \n");
            }
            double x_col = x_pos;
            double y_col = y_pos;
            double dx = x_col - x_pos;
            double dy = y_col - y_pos;
            double dist = sqrt(dx * dx + dy * dy);
            int steps_to_go = 300;
            while (dist < 0.1) {
                steps_to_go--;
                odometry();
                update_sensors();
                follow_wall();
                simulation_step();
                dx = x_col - x_pos;
                dy = y_col - y_pos;
                dist = sqrt(dx * dx + dy * dy);
            }
            vprintf("Moved a little around the obstacle! \n");
        } else {
            // Move towards goal if you are not blocked
            // Stop when too close to an obstacle on a side / front.
            vprintf("Free to go! \n");
            chose_dir = false;
            int steps_to_go = 10;
            while (steps_to_go > 0) {
                steps_to_go--;
                odometry();
                update_sensors();
                vr = VMAX;
                vl = VMAX;
                set_speeds(vl, vr);
                simulation_step();
                if (is_stuck() == STUCK_RIGHT) {
                    vprintf("Stuck to the right when going forwards! \n");
                    steps_to_go = -1;
                    turn(-10);
                } else if (is_stuck() == STUCK_LEFT) {
                    vprintf("Stuck to the right when going forwards! \n");
                    steps_to_go = -1;
                    turn(10);
                }
            }
        }
        if (x_pos < HOME_THRESH && x_pos > -HOME_THRESH &&
            y_pos < HOME_THRESH && y_pos > -HOME_THRESH) {
            robot_close_to_home = true;
        }
    }

    printf("***** Back home ! *****\n");
    printf("***** Done ! *****\n");
    cleanup();
    return EX_SUCCESS;
}

void check_going_home_end(void) {
    if (!robot_running) {
        return;
    }
    #ifdef __WEBOTS__
        process_user_input();
    #endif
    bool robot_close_to_home = false;
    if (prev_x_pos[0] < HOME_THRESH && prev_x_pos[0] > -HOME_THRESH
            && prev_y_pos[0] < HOME_THRESH && prev_y_pos[0] > -HOME_THRESH)
        robot_close_to_home = true;
    if (robot_close_to_home) {
        robot_running = false;
    }
}

void check_follow_wall_end(void) {
    if (!robot_running) {
        return;
    }
    #ifdef __WEBOTS__
        process_user_input();
    #else
        if (simulation_time > TIME_THRESHOLD) {
            robot_running = false;
        }
    #endif
}

void odometry(void) {
    // remember old position
    int i;
    for (i = 0; i < STUCK_TIME - 1; i++) {
        prev_x_pos[i + 1] = prev_x_pos[i];
        prev_y_pos[i + 1] = prev_y_pos[i];
        prev_theta[i + 1] = prev_theta[i];
    }
    prev_x_pos[0] = x_pos;
    prev_y_pos[0] = y_pos;
    prev_theta[0] = theta;
    // sample encoder values
    double encoder_left;
    double encoder_right;
    get_encoders(&encoder_left, &encoder_right);
    // difference in ticks since last sampling
    double ticks_left = encoder_left - prev_encoder_left;
    double ticks_right = encoder_right - prev_encoder_right;
    // convert to meters
    double dist_left = ticks_left / ENCODER_RESOLUTION * WHEEL_RADIUS;
    double dist_right = ticks_right / ENCODER_RESOLUTION * WHEEL_RADIUS;
    double dist_total = (dist_left + dist_right) / 2.0;
    // update values
    theta += (dist_left - dist_right) / AXLE_LENGTH;
    theta = mod(theta, M_2PI);
    x_pos += dist_total * cos(theta);
    y_pos += dist_total * sin(theta);
    prev_encoder_left = encoder_left;
    prev_encoder_right = encoder_right;
    printf("x @ %g cm\n", x_pos * 100);
    printf("y @ %g cm\n", y_pos * 100);
    printf("A @ %g deg\n", rad2deg(theta));
}

void follow_wall(void) {
    double front_sensors  = avg(dsensor[2], dsensor[3]);
    double wall_sensors;
    double pid_sensors;
    double prev_pid_sensors;
    if (follow_right) {
        wall_sensors = avg(dsensor[4], dsensor[5]);
        pid_sensors = dsensor[4];
        prev_pid_sensors = prev_dsensor[4];
    } else {
        wall_sensors = avg(dsensor[0], dsensor[1]);
        pid_sensors = dsensor[1];
        prev_pid_sensors = prev_dsensor[1];
    }
    switch (is_stuck()) {
        case STUCK_RIGHT:
            vr = VTURN;
            vl = -VTURN;
            break;
        case STUCK_LEFT:
            vr = -VTURN;
            vl = VTURN;
            break;
        case NOT_STUCK:
            // wall following
            if (front_sensors < DIST_TO_WALL) {
                vl = VMAX +
                     Kp * (DIST_TO_WALL - pid_sensors) -
                     Kd * (pid_sensors - prev_pid_sensors) / TIME_STEP;
                vl = max(VMIN, vl);
            } else {
                vl = -(VMAX - VMIN);
            }
            // sharpness of turns
            if (wall_sensors < DIST_TO_WALL) {
                vr = VMAX + Kt * (VMAX - vl);
            } else {
                vr = VMAX;
            }
            if (!follow_right) {
                double temp = vl;
                vl = vr;
                vr = temp;
            }
            break;
        default:
            fprintf(stderr, "[%s:%d] unhandled switch case\n",
                    __FILE__, __LINE__);
            exit(EX_UNHANDLED_CASE);
    }
    set_speeds(vl, vr);
}

void follow_move_history(void) {
    if (curr != NULL) {
        vr = -curr->vr;
        vl = -curr->vl;
        curr = curr->next;
        set_speeds(vl, vr);
    } else {
        robot_running = false;
    }
}

int is_stuck(void) {
    bool x_same = true;
    bool y_same = true;
    bool theta_same = true;
    int i;
    for (i = 0; i < STUCK_TIME; i++) {
        x_same = x_same && dbl_eq(x_pos, prev_x_pos[i]);
        y_same = y_same && dbl_eq(y_pos, prev_y_pos[i]);
        theta_same = theta_same && dbl_eq(theta, prev_theta[i]);
        if (!theta_same || !x_same || !y_same) {
            break;
        }
    }
    bool stuck = x_same && y_same && theta_same;
    if (stuck) {
        double right_sensors = avg(dsensor[4], dsensor[5]);
        double left_sensors = avg(dsensor[0], dsensor[1]);
        //if (right_sensors > 0 || left_sensors > 0) {
            if (right_sensors > left_sensors) {
                vprintf(">>> stuck right <<<\n");
                set_led(LED_RIGHT, LED_ON);
                collisions_right++;
                return STUCK_RIGHT;
            } else {
                vprintf(">>> stuck left <<<\n");
                set_led(LED_LEFT, LED_ON);
                collisions_left++;
                return STUCK_LEFT;
            }
        //}
    }
    leds_off();
    vprintf(">>> not stuck <<<\n");
    return NOT_STUCK;
}

void leds_off(void) {
    int i;
    for (i = 0; i < NUM_LEDS; i++) {
        set_led(i, LED_OFF);
    }
}

void set_led(int led_num, int value) {
    #ifdef __WEBOTS__
        wb_led_set(led[led_num], value);
    #else
        fprintf(ROBOT, "L,%d,%d\n", led_num, value);
        fscanf(ROBOT, "l\n");
    #endif
}

void enable_pen(int color) {
    #ifdef __WEBOTS__
    #ifdef __PEN__
        wb_pen_set_ink_color(pen, color, 1.0);
        wb_pen_write(pen, true);
    #endif
    #endif
}

void disable_pen(void) {
    #ifdef __WEBOTS__
    #ifdef __PEN__
        wb_pen_write(pen, false);
    #endif
    #endif
}

void get_encoders(double* encoder_left, double* encoder_right) {
    #ifdef __WEBOTS__
        *encoder_left = wb_differential_wheels_get_left_encoder();
        *encoder_right = wb_differential_wheels_get_right_encoder();
    #else
        fprintf(ROBOT, "H\n");
        fscanf(ROBOT, "h,%lf,%lf\n", encoder_left, encoder_right);
    #endif
    vprintf("<revs-L> %g\n", *encoder_left - prev_encoder_left);
    vprintf("<revs-R> %g\n", *encoder_right - prev_encoder_right);
}

void remember_move(void) {
    Node* temp = (Node*) malloc(sizeof(Node));
    temp->vl = vl;
    temp->vr = vr;
    temp->next = head;
    head = temp;
}

void set_speeds(double left, double right) {
    #ifdef __WEBOTS__
        wb_differential_wheels_set_speed(left, right);
    #else
        fprintf(ROBOT, "D,%lf,%lf\n", left, right);
        fscanf(ROBOT, "d\n");
    #endif
    vprintf("vl => %g\n", left);
    vprintf("vr => %g\n", right);
}

void set_encoders(double left, double right) {
    #ifdef __WEBOTS__
        wb_differential_wheels_set_encoders(left, right);
    #else
        fprintf(ROBOT, "G,%lf,%lf\n", left, right);
        fscanf(ROBOT, "g\n");
    #endif
}

void init(void) {
    int i;
    #ifdef __WEBOTS__
        wb_robot_init();
        // enable shaft encoders
        wb_differential_wheels_enable_encoders(TIME_STEP);
        char sensor_name[4];
        // enable distance sensors
        for (i = 0; i < NUM_DISTANCE_SENSORS; i++) {
            sprintf(sensor_name, "ds%d", i);
            ds[i] = wb_robot_get_device(sensor_name);
            wb_distance_sensor_enable(ds[i], TIME_STEP);
        }
        // enable light sensors
        for (i = 0; i < NUM_LIGHT_SENSORS; i++) {
            sprintf(sensor_name, "ls%d", i);
            ls[i] = wb_robot_get_device(sensor_name);
            wb_light_sensor_enable(ls[i], TIME_STEP);
        }
        // enable LEDs
        char led_name[5];
        for (i = 0; i < NUM_LEDS; i++) {
            sprintf(led_name, "led%d", i);
            led[i] = wb_robot_get_device(led_name);
        }
        // enable pen
        #ifdef __PEN__
            pen = wb_robot_get_device("pen");
        #endif
        // enable keyboard
        #ifdef __KEYBOARD__
            wb_robot_keyboard_enable(TIME_STEP);
        #endif
    #else
        ROBOT = fopen(ROBOT_FILE, "rw");
    #endif
    for (i = 0; i < NUM_DISTANCE_SENSORS; i++) {
        dsensor[i] = prev_dsensor[i] = 0.0;
    }
    for (i = 0; i < NUM_LIGHT_SENSORS; i++) {
        lsensor[i] = 0.0;
    }
    x_pos = y_pos = theta = 0.0;
    for (i = 0; i < STUCK_TIME; i++) {
        prev_x_pos[i] = 0.0;
        prev_y_pos[i] = 0.0;
        prev_theta[i] = 0.0;
    }
    prev_encoder_left = prev_encoder_right = 0.0;
    set_encoders(0.0, 0.0);
    leds_off();
    head = NULL;
    robot_running = true;
    follow_right = PREFER_RIGHT;
    simulation_time = 0;
    collisions_left = collisions_right = 0;
}

void cleanup(void) {
    set_speeds(0.0, 0.0);
    disable_pen();
    #ifdef __WEBOTS__
        wb_robot_cleanup();
    #else
        fclose(ROBOT);
    #endif
    // release linked list
    Node* temp = NULL;
    curr = head;
    while (curr != NULL) {
        temp = curr;
        curr = curr->next;
        free(temp);
    }
    head = curr = NULL;
}

void update_sensors(void) {
    #ifdef __UNSTABLE_CONTROLLER__
        if (simulation_time % (TIME_STEP * 4) != 0) {
            return;
        }
    #endif
    update_distance_sensors();
    // update_light_sensors();
}

void update_light_sensors(void) {
    int i;
    #ifdef __WEBOTS__
        for (i = 0; i < NUM_LIGHT_SENSORS; i++) {
            lsensor[i] = wb_light_sensor_get_value(ls[i]);
        }
    #else
        double l1, l2, f1, f2, r1, r2, b1, b2;
        fprintf(ROBOT, "O\n");
        fscanf(ROBOT, "o,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
               &l1, &l2, &f1, &f2, &r1, &r2, &b1, &b2);
        lsensor[0] = l1; lsensor[1] = l2;
        lsensor[2] = f1; lsensor[3] = f2;
        lsensor[4] = r1; lsensor[5] = r2;
        lsensor[6] = b1; lsensor[7] = b2;
    #endif
    for (i = 0; i < NUM_LIGHT_SENSORS; i++) {
        vprintf("[ls%d] %g\n", i, lsensor[i]);
    }
}

void update_distance_sensors(void) {
    int i;
    for (i = 0; i < NUM_DISTANCE_SENSORS; i++) {
        prev_dsensor[i] = dsensor[i];
    }
    #ifdef __WEBOTS__
        for (i = 0; i < NUM_DISTANCE_SENSORS; i++) {
            dsensor[i] = wb_distance_sensor_get_value(ds[i]);
        }
    #else
        double l1, l2, f1, f2, r1, r2, b1, b2;
        fprintf(ROBOT, "N\n");
        fscanf(ROBOT, "n,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
               &l1, &l2, &f1, &f2, &r1, &r2, &b1, &b2);
        dsensor[0] = l1; dsensor[1] = l2;
        dsensor[2] = f1; dsensor[3] = f2;
        dsensor[4] = r1; dsensor[5] = r2;
        dsensor[6] = b1; dsensor[7] = b2;
    #endif
    for (i = 0; i < NUM_DISTANCE_SENSORS; i++) {
        vprintf("(ds%d) %g\n", i, dsensor[i]);
    }
}

void process_user_input(void) {
    #ifdef __WEBOTS__
    #ifdef __KEYBOARD__
        int key = wb_robot_keyboard_get_key();
        if (!key) {
            return;
        }
        vprintf("!!! KeyPress '%c' !!!\n", (char) key);
        switch (key) {
            case 'X':
            case 'x':
                robot_running = false;
                break;
            default:
                break;
        }
    #endif
    #endif
}

void simulation_step(void) {
    vprintf("------------ t = %ld -------------\n", simulation_time);
    #ifdef __WEBOTS__
        if (wb_robot_step(TIME_STEP) == -1) {
            cleanup();
            fprintf(stderr, "[%s:%d] step interrupt\n",
                    __FILE__, __LINE__);
            exit(EX_INTERRUPT);
        }
    #else
        if (sleep(TIME_STEP / 1000) > 0) {
            cleanup();
            fprintf(stderr, "[%s:%d] sleep interrupt\n",
                    __FILE__, __LINE__);
            exit(EX_INTERRUPT);
        }
    #endif
    simulation_time += TIME_STEP;
}

void turn(double degrees) {
    // positive value of degrees = clockwise rotation
    // use relationship between encoder revolutions and distance
    // distance for every wheel to go
    double dist = M_PI * AXLE_LENGTH / 360.0 * fabs(degrees);
    // ticks to go
    double ticks =  dist * ENCODER_RESOLUTION / WHEEL_RADIUS;
    double ticks_made = 0;
    double encoder_init_left;
    double encoder_init_right;
    double encoder_new_left;
    double encoder_new_right;
    get_encoders(&encoder_init_left, &encoder_init_right);
    while (ticks_made < ticks) {
        // keep track of position
        odometry();
        // set wheel speed
        if (degrees > 0 ) {
            vr = - VTURN;
            vl = VTURN;
        } else {
            vr = VTURN;
            vl = - VTURN;
        }
        set_speeds(vl, vr);
        // make a step
        simulation_step();
        // keep track of the ticks we made
        get_encoders(&encoder_new_left, &encoder_new_right);
        if (encoder_new_left >= encoder_init_right) {
            ticks_made = encoder_new_left - encoder_init_left;
        } else {
            ticks_made = encoder_init_left - encoder_new_left;
        }
    }
    vprintf("Finished rotating %f degrees\n", degrees);
}

void forwards(double meters) {
    // use relationship between encoder revolutions and distance
    // ticks to go
    double ticks =  meters * ENCODER_RESOLUTION / WHEEL_RADIUS;
    if (meters < 0) {
        ticks = -ticks;
    }
    double ticks_made = 0;
    double encoder_init_left;
    double encoder_init_right;
    double encoder_new_left;
    double encoder_new_right;
    get_encoders(&encoder_init_left, &encoder_init_right);
    while (ticks_made < ticks) {
        // keep track of position
        odometry();
        // set wheel speed
        if (meters > 0) {
            vr = VMAX;
            vl = VMAX;
        } else {
            vr = -VMAX;
            vl = -VMAX;
        }
        set_speeds(vl, vr);
        // make a step
        simulation_step();
        // keep track of the ticks we made
        get_encoders(&encoder_new_left, &encoder_new_right);
        if (encoder_new_left >= encoder_init_right) {
            ticks_made = encoder_new_left - encoder_init_left;
        } else {
            ticks_made = encoder_init_left - encoder_new_left;
        }
    }
    vprintf("Finished moving %f meters\n", meters);
}

void faceHome(void) {
    double current_angle = ((int) rad2deg(theta)) % 360;

    if (current_angle > 180) {
        current_angle = - 360 + current_angle;
    } else if (current_angle < -180) {
        current_angle = 360 + current_angle;
    }
    vprintf("Angle the robot has before: %f \n", current_angle);

    // angle the robot should have in order to face the origin
    double goal_angle = 0;

    // determine quadrant robot is in
    if (x_pos >= 0 && y_pos >= 0) {
        vprintf("First quadrant \n");
        goal_angle = - 180 + rad2deg(atan( fabs(y_pos) / fabs(x_pos) ));
    } else if (x_pos < 0 && y_pos >= 0) {
        vprintf("Second quadrant \n");
        goal_angle = - rad2deg(atan( fabs(y_pos) / fabs(x_pos) ));
    } else if (x_pos < 0 && y_pos < 0) {
        vprintf("Third quadrant \n");
        goal_angle = rad2deg(atan( fabs(y_pos) / fabs(x_pos) ));
    } else if (x_pos >= 0 && y_pos < 0) {
        vprintf("Fourth quadrant \n");
        goal_angle = 180 - rad2deg(atan( fabs(y_pos) / fabs(x_pos) ));
    }

    vprintf("Angle the robot should have: %f \n", goal_angle);
    vprintf("Diff init: %f \n", fabs(goal_angle - current_angle));

    bool dir = true;
    if (trig_dist(current_angle, goal_angle) > 180) {
        dir = false;
    }

    while (fabs(goal_angle - current_angle) > 2) {
        // keep track of position
        odometry();
        // set wheel speeds
        if (dir) {
            vr = - VTURN;
            vl = VTURN;
        } else {
            vr = VTURN;
            vl = - VTURN;
        }
        set_speeds(vl, vr);
        // make a step
        simulation_step();
        // update angle
        current_angle = ((int) rad2deg(theta)) % 360;
        if (current_angle > 180) {
            current_angle = - 360 + current_angle;
        } else if (current_angle < -180) {
            current_angle = 360 + current_angle;
        }
    }
    vprintf("Angle the robot has after: %f \n", current_angle);
}
